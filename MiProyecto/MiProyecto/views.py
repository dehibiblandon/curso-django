from django.http import HttpResponse
from django.template import Template, Context
import datetime
from django.template.loader import get_template
from django.shortcuts import render
# Request para realizar peticiones al server
#HttpResponse: Para enviar la respuesta usando el http

def bienvenida(request): #pasamos un objeto de tipo request
    return HttpResponse("Bienvenid@ al curso de django")

def bienvenidaRED(request): #pasamos un objeto de tipo request
    return HttpResponse("<h2 style='color: red;'>Bienvenid@ al curso de django</h2>")

def categoriaEdad(request, edad):
    if edad >= 18:
        if edad >= 60:
            categoria = "Tercera edad"
        else:
            categoria = "Adultez"
    else:
        if edad < 10:
            categoria = "Infancia"
        else:
            categoria = "Adolescencia"
    resultado = "<h1> Categoría de la edad: %s</h1>" %categoria
    return HttpResponse(resultado)

def contenidoHtml (request, nombre, edad):
    contenido= """
    <html>
    <body>
    <p>Nombre: %s /Edad: %s </p>
    </body>
    </html>
    """ %(nombre, edad)
    return HttpResponse (contenido)

def plantilla1 (request):
    ##abrimos el documento que contiene la plantilla
    plantillaExt = open("C:/Users/DehibiBlandon/Documents/Django/MiProyecto/MiProyecto/plantillas/plantilla1.html")
    ##cargamos el documento en una variable de tipo Template
    template = Template(plantillaExt.read())
    ##cerrar el documento externo que hemos abiero
    plantillaExt.close()
    #crear contexto donde se trabaran las variables
    contexto = Context()
    #Renderizar el documento
    documento = template.render(contexto)
    return HttpResponse(documento)

def plantillaParametros (request):
    """
    Este es un comentario de varias
    lineas en python 
    puro
    """
    ##variables a enviar
    nombre = "Dehibi Blandon Curso"
    fechaActual = datetime.datetime.now()
    lenguajes = ["python", "Ruby" "Javascript", "java", "C#"]
    ##abrimos el documento que contiene la plantilla
    plantillaExt = open("C:/Users/DehibiBlandon/Documents/Django/MiProyecto/MiProyecto/plantillas/pantillaParametros.html")
    ##cargamos el documento en una variable de tipo Template
    template = Template(plantillaExt.read())
    ##cerrar el documento externo que hemos abiero
    plantillaExt.close()
    #crear contexto donde se trabaran las variables
    ##Envio de parametros al template
    contexto = Context({"nombreCanal": nombre, "fechaActual":fechaActual, "lenguajes": lenguajes})
    #Renderizar el documento
    documento = template.render(contexto)
    return HttpResponse(documento)

def plantillaLoaders (request):
    #Obtener las plantillas por el método loader y la ruta de carga de las plantillas
    # se configuran en el archivo settings apartado TEMPLATES seccion DIRS
    nombre = "Dehibi Blandon Curso"
    fechaActual = datetime.datetime.now()
    lenguajes = ["python", "Php", "Angular", "java", "C#"]
    plantillaExterna = get_template('pantillaParametros.html')
    documento = plantillaExterna.render({"nombreCanal": nombre, "fechaActual":fechaActual, "lenguajes": lenguajes})
    return HttpResponse(documento)

def platillaShortCut(request):
    nombre = "Dehibi Blandon Curso"
    fechaActual = datetime.datetime.now()
    lenguajes = ["python", "React", "Angular", "Laravel", "C#"]
    # requiere parametros, lo que es el request, la plantilla, el contexto
    return render(request, 'pantillaParametros.html', {"nombreCanal": nombre, "fechaActual":fechaActual, "lenguajes": lenguajes})

def plantillaVista1(request):
    return render(request, 'vista1.html')

def plantillaVista2(request):
    return render(request, 'vista2.html')
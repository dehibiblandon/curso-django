"""MiProyecto URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from MiProyecto.views import bienvenida, bienvenidaRED
from MiProyecto.views import categoriaEdad
from MiProyecto.views import contenidoHtml
from MiProyecto.views import plantilla1, plantillaParametros
from MiProyecto.views import plantillaLoaders, platillaShortCut
from MiProyecto.views import plantillaVista1, plantillaVista2

urlpatterns = [
    path('admin/', admin.site.urls),
    path('welcome/', bienvenida),
    path('welcomered/', bienvenidaRED),
    path('categoria/<int:edad>', categoriaEdad),
    path('home/<nombre>/<int:edad>', contenidoHtml),
    path('plantilla1/', plantilla1),
    path('plantilla2/', plantillaParametros),
    path('plantilla3/', plantillaLoaders),
    path('plantilla4/', platillaShortCut),
    path('palntillaVista1/', plantillaVista1),
    path('plantillaVista2/', plantillaVista2),
    
]
